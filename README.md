# Portafolio
Acá se muestran todos los proyectos que hice a lo largo del tiempo, principalmente proyectos de CADe Simu y Autocad.

# CADe Simu

## Arranque, parada y cambio de giro en Logo Siemens
Este fue el último proyecto escolar de la Capacitación laboral.
Consta de 2 botoneras y 1 switch de cambio de giro, y de protección 2 relés térmicos.

Al presionar el botón de arranque y mientras que no esté algún relé térmico activo encenderá, dependiendo del sentido de giro del switch.

Al hacer el cambio de giro del motor mientras este esté funcionando se activará un timer de **5s** para evitar un cambio muy brusco y que se funda.

Y finalmente al parar el motor este lo hará, manteniendo en "memoria" el sentido de giro antes mencionado. Si surge algun problema con algún relé térmico y se soluciona de manera automática o por intervención del operario, este volverá a funcionar sin problemas, para ello se deberá presionar la botonera de parar. Ideal para ciclos que no se puedan detener o que el motor esté expuesto a altas temp. durante un corto periodo de tiempo.

## Imágenes
![alt text](CADe_Simu/Arranque_logo/1.png "Repaso general, estado de repaso")

![alt text](CADe_Simu/Arranque_logo/2.png "Estado de arranque")

![alt text](CADe_Simu/Arranque_logo/3.png "Estado de cambio de giro")

![alt text](CADe_Simu/Arranque_logo/3.png "Estado de problema en el 2do relé térmico.")

## Vídeos
**Próximamente...**

## Moto compresor simple
Circuito básico

La ficha selectora tiene 2 modos, on y off, al dejarla on el motor comenzará a andar, hasta que se ponga off manualmente o salte el relé térmico por sobre calentamiento o otro factor.

## Imágenes
![alt text](CADe_Simu/Motocompresor/1.png "General")
![alt text](CADe_Simu/Motocompresor/2.png "Andando")
![alt text](CADe_Simu/Motocompresor/3.png "Relevo térmico")

## Vídeos
**Próximamente..**

## Horno electrico trifásico concepto.
- **En Revisión**
Concepto ídeado en clase sobre el funcionamiento de un horno de panadería. Poseé algunos bugs y fallos, falta revisar y depurar.

Al encenderlo desde la perilla selectora, esta encenderá las 3 resistencias electricas y el motor que hace girar sobre su propio eje la bandeja.

Al alcanzar cierta temperatura, un contacto del termostato se abre y cierra otra parte del circuito, conectando un motor que ventila el área, hasta que el termostato vuelva a cerrar. Cómo depende de la temperatura, este contacto se puede cambiar por algún temporizador sin problemas.
## Imágenes

![alt text](CADe_Simu/Horno_Electrico/1.png "General")
![alt text](CADe_Simu/Horno_Electrico/2.png "Funcionando las resistencias")
![alt text](CADe_Simu/Horno_Electrico/3.png "Funcionando la ventilación y el motor de las bandejas.")
![alt text](CADe_Simu/Horno_Electrico/4.png "Motor de las bandejas con sobre calientamiento.")

## Vídeos

**Próximamente**

# Autocad - Diseño de planos eléctricos.

# Vivienda de 1 planta.
Por temas de discreción y seguridad no puedo subir el archivo PDF, por que figuran datos de mi cliente y míos, así que dejo imágenes.

## Imágenes
![alt text](AutoCAD/img/Screenshot_123.png "Distribución del tablero eléctrico.")
![alt text](AutoCAD/img/Screenshot_124.png "Balance de carga.")
![alt text](AutoCAD/img/Screenshot_125.png "General")

# **¿Dudas? WH: 264-5886369**
